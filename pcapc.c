/* vim: set tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab */

/*
	pcapc - libpcap filter expressions compiler
	Copyright (C) 2014-2019, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pcap/pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

int main(int argc, char** argv)
{
	if (argc < 2)
		exit(EX_USAGE);

	unsigned int argl = 0;
	for (int i = 1; i < argc; i++)
		argl += strlen(argv[i]) + 1;
	char expression[argl];
	memset(expression, 0, argl);
	unsigned int offset = 0;
	for (int i = 1; i < argc; i++)
	{
		int written = 0;
		int last = 0;
		if (i == argc - 1)
			last = 1;
		written = snprintf(expression + offset, strlen(argv[i]) + 1 + (last ? 0 : 1), last ? "%s" : "%s ", argv[i]);

		if (written < 0)
			break;
		offset += written;
	}

	struct bpf_program bpf_program;
	pcap_t* pcap = pcap_open_dead(DLT_EN10MB, -1);

	memset(&bpf_program, 0, sizeof(struct bpf_program));

	int res = pcap_compile(pcap, &bpf_program, expression, 1, PCAP_NETMASK_UNKNOWN);
	if (res == 0)
	{
		printf("%u,", bpf_program.bf_len);
		for (unsigned int i = 0; i < bpf_program.bf_len; i++)
		{
			printf("%u %u %u %u,",
				bpf_program.bf_insns[i].code,
				bpf_program.bf_insns[i].jt,
				bpf_program.bf_insns[i].jf,
				bpf_program.bf_insns[i].k);
		}
	} else
	{
		pcap_close(pcap);
		exit(EX_SOFTWARE);
	}

	pcap_close(pcap);

	exit(EX_OK);
}

