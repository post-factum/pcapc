PROG = pcapc
OBJS = pcapc.o
PREFIX ?= /usr/local
CFLAGS = -O3 -Wall -Wextra -pedantic -fstack-protector-strong -fno-plt
LDFLAGS = -lpcap

all: build

build: $(PROG)

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(LDFLAGS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $<

install:
	install -Dm0755 $(PROG) $(DESTDIR)$(PREFIX)/bin/$(PROG)

clean:
	rm -f $(PROG) $(OBJS)

.PHONY: all install clean
