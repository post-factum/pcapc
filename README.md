pcapc
=====

Description
-----------

Small tool to compile libpcap filter expressions into BPF opcodes.

Compiling
---------

### Prerequisites

* make (tested with GNU Make 4.2.1)
* gcc (tested with v9.2.0)
* libpcap (tested with v1.9.1)

### Compiling

Just type `make`.

Usage
-----

Typical usage:

`pcapc ip and dst host 8.8.8.8 and dst port 53 and udp`

The output is intended to be used in conjunction with tc cls\_bpf classifier like this:

`tc filter add dev eth0 parent 1: bpf run bytecode "$(pcapc dst port 22)" flowid 1:10`

Distribution and Contribution
-----------------------------

This software is distributed under terms and conditions of GPLv3. See file `COPYING` for details.

Author: Oleksandr Natalenko aka post-factum &lt;oleksandr@natalenko.name&gt;. Mail me any
comments and suggestions, and send pull requests.

